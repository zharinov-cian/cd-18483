INSERT INTO hive.seo_dev.position_histories_zharinov2
SELECT
    phist.id,
    phist.se_id,
    phist.device,
    phist.query_id,
    phist.region_id,
    phist.site_id,
    phist.position,
    phist.url,
    phist.url_title,
    phist.snippet,
    phist.recorded_at AS recorded_at_mysql,
    phist.parsing_request_id,
    CAST(phist.is_first_for_site AS BOOLEAN),
    CAST(phist.is_virtual AS BOOLEAN),
    phist.viv,
    CAST(now() AS TIMESTAMP) AS recorded_at_presto,
    CAST(YEAR(ppreq.created_at) AS INTEGER) AS request_year,
    CAST(MONTH(ppreq.created_at) AS INTEGER) AS request_month
FROM mysql_seo.gulliver.position_histories AS phist
JOIN
    mysql_seo.gulliver.position_parsing_requests AS ppreq
    ON ppreq.id = phist.parsing_request_id
WHERE
    phist.id >= $N * 1000 * 1000
    AND phist.id < ($N + 1) * 1000 * 1000
;
