#!/bin/bash

source env.sh

./presto-cli-0.189-executable.jar \
    --server 192.168.177.251:8080 \
    --user $PRESTO_USER \
    --catalog hive \
    --schema seo_dev \
    --output-format ALIGNED \
    --file "$1"
