-- Список загруженных диапазонов с количеством записей в них

SELECT
    CAST(id / (1000 * 1000) AS INTEGER) AS id_interval,
    COUNT(id) AS rows_number
FROM seo_dev.position_histories_zharinov2
GROUP BY CAST(id / (1000 * 1000) AS INTEGER)
ORDER BY id_interval
;
