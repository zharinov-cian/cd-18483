#!/bin/bash

for N in $(seq 0 300)
do
    echo "N = $N  $(date +%H:%M:%S)";
    SQL="$(cat m2p_intervals.sql.tpl | envsubst)";
    
    ./presto-cli-0.189-executable.jar \
        --server 192.168.177.251:8080 \
        --user azharinov \
        --catalog hive \
        --schema seo_dev \
        --execute "$SQL" \
    || exit 1 \
    ;

done

